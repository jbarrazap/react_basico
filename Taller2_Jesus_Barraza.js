//Leer un número entero y determinar si es negativo.

let numero =9;

if(numero>0){
    console.log("El numero es positivo");
}else{
    console.log("El numero es negativo");
}

//Leer un número entero de dos digitos y terminar a cuánto es igual la suma de sus digitos.

let numero1 = 35;

if (numero1 >= 10 && numero1 <= 99) {
  const digito1 = parseInt(numero1 / 10);
  const digito2 = parseInt(numero1 % 10);

  const suma = digito1 + digito2;

  // const mensaje = `La suma de ${digito1} + ${digito2} = ${suma}`;

  //const mensaje = 'La suma de ' + digito1 + ' + ' + digito2 + ' = ' + suma;

  console.log('La suma de', digito1, '+', digito2, '=', suma);
  
} else {
  console.log('El numero no cumple con los requisitos!');
}
//Leer un número entero de dos digitos y determinar si ambos digitos son pares.

let numero2 = 48;

if (numero2 >= 10 && numero2 <= 99) {
  const digito1 = parseInt(numero2 / 10);
  const digito2 = parseInt(numero2 % 10);

  if(digito1%2==0 && digito2%2==0){

    console.log('Los digitos: ', digito1, ' y ', digito2, ' son pares.');

  }else{

    console.log('Los digitos: ', digito1, ' y ', digito2, ' NO son pares.');

  }

} else {
  console.log('El numero no cumple con los requisitos!');
}

//Leer un número entena de dos digitos menor gue 20 y determinar si es primo.

let numero3 = 6;

if (numero3 >= 1 && numero3 <= 20) {
  let cont=0;
    for(let i=1;i<=numero3;i++){   
        if(numero%i==0){
            cont++
        }
    }
    if(cont==2){
        console.log('El numero es primo')
    }else{
        console.log('El numero NO es primo')
    }
    cont=0;
} else {
  console.log('El numero no cumple con los requisitos!');
}

//Leer un número entena de dos digitos y determinar si es primo y además si es negativo.

let numero3 = 6;

if (numero3 >= 1 && numero3 <= 100) {
    evaluar_numero(numero3);
} else {
  console.log('El numero no cumple con los requisitos!');
}

function evaluar_numero(numero){

  let cont=0;
    for(let i=1;i<=numero3;i++){   
        if(numero%i==0){
            cont++
        }
    }
    if(cont==2){
        console.log('El numero es primo');
    }else{
        console.log('El numero NO es primo');
    }
    cont=0;

    if(numero<0){
      console.log("El numero es negativo.");
    }else{
      console.log("El numero NO es negativo");
    }

  }

//Leer un número enteros de dos digitos y determinar si un digito es múltiplo del otro.

let numero4 = 63;

if(numero4<=10){
  console.log("Ingrese un número entero de dos digitos: ");
}

multiplo(numero4);

function multiplo(numero){
    
    for(let i=1;i<=10;i++){ 
        
        if(numero%10==Math.trunc(numero/10)*i){
            console.log("Los numeros ",(numero%10)," y ",Math.trunc(numero/10)," son multiplos");
            }else if(Math.trunc(numero/10)==numero%10*i){
                console.log("Los numeros ",(numero%10)," y ",Math.trunc(numero/10)," son multiplos");
            }
    }
}

//Leer un número entero de dos digitos y determinar si los dos digitos son iguales.
let numero5 = 96;

if(numero5<10 || numero5>99){
  console.log("El numero no es valido, ingrese un número entero de dos digitos: ");
}

iguales(numero5);

function iguales(numero){
        
        if(numero%10==Math.trunc(numero/10)){
            console.log("Los numeros ",(numero%10)," y ",Math.trunc(numero/10)," son iguales");
            }else{
                console.log("Los numeros ",(numero%10)," y ",Math.trunc(numero/10)," NO son iguales");

            }
}

//Leer un número entero de 4 digitos y determinar si el segundo digito es igual al penúltimo. 

let validacion=true;

while(validacion) {

  var numero5 = prompt("Digite un numero de 4 digitos:  ");

  if(numero5<1000 || numero5>9999){
    console.log("El numero no es valido, ingrese un número entero de cuatro digitos: ");
  }else{
      validacion=false;
  }

}


iguales(numero5);

function iguales(numero){

        let num_1 = Math.trunc((numero%1000)/100);
        //console.log(num_1);
        let num_2 = Math.trunc((numero%100)/10);
        //console.log(num_2);

        if(num_1==num_2){
            console.log("Los numeros ",num_1," y ",num_2," son iguales");
            }else{
                console.log("Los numeros ",num_1," y ",num_2," NO son iguales");

            }
}

//Leer tres numeros enteros y determinar si el último digito. de los tres numeros es igual.

let numero_1= parseInt(prompt("Digite el numero #1: "));
let numero_2= parseInt(prompt("Digite el numero #2: "));
let numero_3= parseInt(prompt("Digite el numero #3: "));



ultimo_digito_iguales(numero_1, numero_2, numero_3);

function ultimo_digito_iguales(numero_1, numero_2, numero_3){

        let num_1 = numero_1%10;
        console.log(num_1);
        let num_2 = numero_2%10;
        console.log(num_2);
        let num_3 = numero_3%10;
        console.log(num_3);

        if(num_1==num_2 && num_2==num_3){
            console.log("Los numeros ",num_1,", ",num_2," y ",num_3," son iguales");
            }else{
                console.log("Los numeros ",num_1,", ",num_2," y ",num_3," NO son iguales");

            }
}

//Leer dos numeros enteros y determinar si la diferencia entre los dos es un número divisor 
//exacto de alguno de los dos números.


let numero_a=prompt("Digite el primer numero: ");
let numero_b = prompt("Digite el segundo numero: ");

diferencia_divisor(numero_a, numero_b);

function diferencia_divisor(numero_a, numero_b){

  let resta = numero_a-numero_b;

  if(numero_a%resta==0){
      console.log("La diferencia: ",resta," es divisor exacto del numero: ",numero_a);
  }else if(|| numero_b%resta==0){
    console.log("La diferencia: ",resta," es divisor exacto del numero: ",numero_b);
  }else{
    console.log("La diferencia: ",resta," No es divisor exacto de ninguno d elos numeros: ",numero_a," y ",numero_b);
  }


}


//Leer un número entero y determinar a cuánto es igual la suma de sus digitos.

let variable_1 = parseInt(prompt("Digite un número entero: "));

suma_digitos(variable_1);

function suma_digitos(numero){

  let suma=0;
  while(numero/10>0){
    suma=parseInt(suma+(numero%10));
    numero=numero/10;
  }
  console.log("la suma de sus digitos es: ",suma);
}

//Leer un número entero y determinar cuantas veces tiene el digito 1

let variable_1 = prompt("Digite un número entero:");

numero_uno(variable_1);

function numero_uno(numero){

  let cont=0;
  while(numero!=0){
    
    if(parseInt(numero%10)==1){
      cont=cont+1;
    }
    
    numero=parseInt(numero/10);
    
  }

  console.log("El numero contiene el numero 1, ",cont," vez/veces.");
}
//Generar todas las tablas de multiplicar del 1 al 10

console.log("Tablas de Multiplicar del 1 al 10.");
for(let i=1; i<=10;i++){
  console.log("Tabla del ",i,".");
  for(let j=1;j<=10;j++){
    console.log(i," * ",j," = ",i*j);
  }
  console.log("----------------------")
}

//Leer un número entero y mostrar en pantalla su tabla de multiplicar

numero=prompt("Ingrese un numero:")

console.log("La tabla de Multiplicar del numero:",numero);

for(let i=1; i<=10;i++){
  
  console.log(numero," * ",i," = ",numero*i);
  
}

//Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se 
//encuentra el número mayor.

let vector=[];
let mayor=0;
let posicion;

for(let i=0;i<10;i++){
  vector[i]=prompt("ingrese el numero de la posicion ",i+1);
}

for(let i=0;i<10;i++){
  console.log(vector[i]);
  if(vector[i]>mayor){
    mayor=vector[i];
    posicion=i;
  }
}

console.log("la posicion donde se encuentra el numero mayor es: ",posicion+1);


//Leer 10 números enteros, almacenarlos en un vector y determinar en qué posiciones se 
//encuentran los números terminados en O

let vector=[];

for(let i=0;i<10;i++){
  vector[i]=prompt("ingrese el numero de la posicion ",i+1);
}

for(let i=0;i<10;i++){
  console.log(vector[i]);}

for(let i=0;i<10;i++){
  
  if(vector[i]%10==0){
    console.log("El numero ",vector[i],"en la posicion ",i+1," termina en 0.");
  }
}

//Leer 10 números enteros, almacenados en un vector y determinar cuántas veces está 
//repetido el mayor

let vector=[];
let mayor=0;
let cont=0;

for(let i=0;i<10;i++){
  vector[i]=prompt("ingrese el numero de la posicion ",i+1);
}

for(let i=0;i<10;i++){
  console.log(vector[i]);
  if(vector[i]>=mayor){
    mayor=vector[i];
  }
}

for(let i=0;i<10;i++){
    if(vector[i]==mayor){
    cont=cont+1;
  }
}

console.log("mayor: ",mayor);
console.log("contador: ",cont);

if(cont>1){
  console.log("El numero mayor es: ",mayor,", se repite: ",cont," veces.");
}else{
  console.log("El numero mayor es: ",mayor," y no se repite");
}
